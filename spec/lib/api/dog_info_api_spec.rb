# frozen_string_literal: true

describe DogInfoApi do
  describe '#breeds' do
    let(:breeds) { file_fixture('breeds.json').read }

    it 'returns breed results' do
      stub_request(:get, 'https://api.thedogapi.com/v1/breeds')
        .to_return(status: 200, body: breeds, headers: {})
      client = described_class.new(url: ENV['DOG_URL'], api_key: ENV['DOG_KEY'])
      expect(client.breeds.first[:name]).to eq 'Wirehaired Vizsla'
    end

    it 'fails with message when malformed credentials' do
      client = described_class.new(url: 'sdfsdf', api_key: 'sdfsdf')
      expect { client.breeds }.to raise_error(DogInfoApi::ApiError, 'Error occured: bad URI(is not URI?): "://"')
    end

    it 'fails with message on timeout' do
      stub_request(:get, 'https://api.thedogapi.com/v1/breeds').to_timeout
      client = described_class.new(url: ENV['DOG_URL'], api_key: ENV['DOG_KEY'])
      expect { client.breeds }.to raise_error(DogInfoApi::ApiError, 'Error occured: execution expired')
    end
  end
end
