# frozen_string_literal: true

describe ImageRecognition do
  describe '#image_detection' do
    let(:dog_response) { JSON.parse(file_fixture('google_response.json').read) }

    it 'returns image results' do
      client = described_class.new(fixture_file_upload('/files/dog.jpeg', 'image/jpg').path)
      VCR.use_cassette('googleapi_oauth2') do
        expect(client.image_detection).to eq(dog_response)
      end
    end
  end
end
