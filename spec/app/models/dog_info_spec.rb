# frozen_string_literal: true

describe DogInfo do
  let(:dog) { { 'Dog' => 0.9953705072402954, 'Retriever' => 0.9020378589630127 } }
  let(:dog_but_not_sure) { { 'Dog' => 0.9953705072402954, 'rare breed' => 0.9 } }
  let(:cat) { { 'cat' => 0.9953705072402954 } }

  describe '#dog?' do
    it 'returns true if the image has dog' do
      expect(described_class.dog?(dog)).to eq true
    end

    it 'returns false if the image hasn\'t got the dog' do
      expect(described_class.dog?(cat)).to eq false
    end
  end

  describe '#breed_info' do
    it 'returns not dog' do
      expect(described_class.breed_info(cat)).to eq({ "That's probably not a dog, but I sniffed:" => ['cat'] })
    end

    it 'returns dog but not sure' do
      stub_request(:get, 'https://api.thedogapi.com/v1/breeds')
        .to_return(status: 200, body: [life_span: '12 - 16 years', name: 'Retriever'].to_json, headers: {})
      message = 'This is a dog, but I can\'t tell which breed, must be rare, I sniffed:'
      expect(described_class.breed_info(dog_but_not_sure)).to eq message => ['rare breed']
    end

    it 'returns dog' do
      stub_request(:get, 'https://api.thedogapi.com/v1/breeds')
        .to_return(status: 200, body: [life_span: '12 - 16 years', name: 'Retriever'].to_json, headers: {})

      expect(described_class.breed_info(dog)).to eq 'Retriever' => { life_span: '12 - 16 years', name: 'Retriever' }
    end

    it 'returns image results if something goes wrong with breed provider' do
      stub_request(:get, 'https://api.thedogapi.com/v1/breeds')
        .to_return(status: 404, body: { 'message': 'Not Found' }.to_json, headers: {})
      message = 'This is a dog, but I can\'t tell which breed, must be rare, I sniffed:'
      expect(described_class.breed_info(dog)).to eq message => ['Retriever']
    end
  end

  describe '#breeds_that_match' do
    it 'return breed info on match' do
      stub_request(:get, 'https://api.thedogapi.com/v1/breeds')
        .to_return(status: 200, body: [life_span: '12 - 16 years', name: 'Retriever'].to_json, headers: {})
      expect(described_class.breeds_that_match('Retriever')).to eq 'Retriever' => { life_span: '12 - 16 years', name: 'Retriever' }
    end

    it 'doesn\'t find the breed' do
      stub_request(:get, 'https://api.thedogapi.com/v1/breeds')
        .to_return(status: 200, body: [life_span: '12 - 16 years', name: 'Retriever'].to_json, headers: {})
      expect(described_class.breeds_that_match('Noodle')).to eq({})
    end
  end
end
