# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DogController, type: :controller do
  before do
    credentials = ActionController::HttpAuthentication::Basic.encode_credentials 'demo', 'secret'
    request.env['HTTP_AUTHORIZATION'] = credentials
  end

  describe '#upload' do
    it 'returns image details' do
      VCR.use_cassette('googleapi_oauth2') do
        post :upload, params: { "image": fixture_file_upload('/files/dog.jpeg', 'image/jpg') }
        expect(response).to have_http_status(:success)
        expect(response).to render_template(:index)
      end
    end

    it 'returns error if something goes wrong' do
      stub_request(:post, 'https://www.googleapis.com/oauth2/v4/token').to_timeout
      post :upload, params: { "image": fixture_file_upload('/files/dog.jpeg', 'image/jpg') }
      parsed_response = JSON.parse(response.body, symbolize_names: true)
      expect(response).to have_http_status(:internal_server_error)
      expect(parsed_response).to eq message: 'Something went wrong try again later'
    end
  end
end
