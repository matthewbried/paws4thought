# frozen_string_literal: true

require 'google/cloud/vision'

class ImageRecognition
  def initialize(image)
    @image = image
  end

  def image_detection
    image_annotator = Google::Cloud::Vision::ImageAnnotator.new
    detect_obj = image_annotator.label_detection image: @image

    result = {}
    detect_obj.responses.each do |response|
      response.label_annotations.each do |label|
        result[label.description] = label.score
      end
    end
    result
  end
end
