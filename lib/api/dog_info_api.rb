# frozen_string_literal: true

class DogInfoApi
  class ApiError < StandardError
  end

  def initialize(url:, api_key:)
    @url = url
    @api_key = api_key
  end

  def breeds
    response = connection.get '/v1/breeds'
    JSON.parse(response.body, symbolize_names: true)
  rescue StandardError => e
    raise ApiError, "Error occured: #{e.message}"
  end

  def connection
    Faraday.new(url: @url, ssl: { verify: false }) do |conn|
      conn.headers['x-api-key'] = @api_key
      conn.headers['Content-Type'] = 'application/json'
      conn.adapter Faraday.default_adapter
    end
  end
end
