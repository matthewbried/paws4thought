FROM ruby:2.7.2-slim-buster

ENV RUNTIME_DEPS="curl less bash tzdata git nodejs" \
    BUILD_DEPS="build-essential libpq-dev libxml2-dev libxslt-dev"

WORKDIR /app

COPY Gemfile* ./

RUN apt-get update && apt-get install -y $RUNTIME_DEPS \
    && apt-get update && apt-get install -y $BUILD_DEPS \
    && bundle install \
    # give the current user access to the bundle
    && chmod -R o+rwx /usr/local/bundle
WORKDIR /app
COPY . /app
COPY Gemfile /app/Gemfile
COPY Gemfile.lock /app/Gemfile.lock

ENTRYPOINT ["/app/docker-entrypoint.sh"]
# Start the main process.
CMD bundle exec rails s -p $PORT -b 0.0.0.0
