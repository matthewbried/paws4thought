[![pipeline status](https://gitlab.com/matthewbried/paws4thought/badges/master/pipeline.svg)](https://gitlab.com/matthewbried/paws4thought/-/commits/master)
[![coverage report](https://gitlab.com/matthewbried/paws4thought/badges/master/coverage.svg)](https://gitlab.com/matthewbried/paws4thought/-/commits/master)

# K-9 Dog Bot (paws4thought)

Sniff out dogs from uploaded images. Determines the species if it can and gives stats on that species.

## Ruby version
* 2.7.2

## Rails version
* 6.0.3.4

## How it works

Temp uploaded image is passed to googles vision api for label identification. The labels are filtered to attempt to identify a dog. If it determines the dog species it will use thedogapi to pull information for that species otherwise it will just display what labels it identifies.

## Basic Auth login
These creds aren't incredibly sensitive the basic auth is there to deter bots.
For the sake of demo'ing these are the actual credentials. With the changes coming this should phase out.
- https://paws4thought.herokuapp.com/
- user: demo
- pass: Top_Secret_Code!

##
The google credentials are false in the repo and the key.json will need to be updated for VCR to work locally. 

## TODOs
- Replace VCR stub for google oauth with better stubbing so that mutable expiring token doesn't fail further tests.
- Add validations.
- Create user/customer dog entities in the DB (Customer, Address_Details, Dog)
- Instead of basic auth have the users use their own creds/facebook/google to authenticate
- On success for species identification fetch different image of the same species from the api and display it on the stat card.
- switch materialize for material and improve the frontend.
- Bring up a map with nearest distance to local vets from location of user.
- link domain to heroku app

## Configuration
* environment variables set:
 - GOOGLE_APPLICATION_CREDENTIALS: 'config/key.json'
 - GOOGLE_APPLICATION_CREDENTIALS_CONTENT: '{"type": "service_account",
        "project_id": "",
        "private_key_id": "",
        "private_key": "-----BEGIN PRIVATE KEY-----\n\n-----END PRIVATE KEY-----\n",
        "client_email": "",
        "client_id": "",
        "auth_uri": "",
        "token_uri": "",
        "auth_provider_x509_cert_url": "",
        "client_x509_cert_url": ""}'
 - DOG_KEY: ''
 - DOG_URL: https://api.thedogapi.com/
 - Demo_Pass: secret

## How to run the test suite
 - docker-compose build
 - docker-compose run app rspec spec

## CI/CD
 - .gitlab-ci.yml
 - runs rspec rubocop rubycritic

## Deploy
 - heroku container:push web --app paws4thought
 - heroku container:release web --app paws4thought 
