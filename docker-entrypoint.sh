#!/bin/sh
set -e

if [ -f tmp/pids/server.pid ]; then
  rm tmp/pids/server.pid
fi
bundle exec rake db:prepare
#the below is so that rubycritic passes it's uncommited warning in the pipeline
git update-index -q --really-refres

exec "$@"
