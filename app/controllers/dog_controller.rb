# frozen_string_literal: true

class DogController < ApplicationController
  def upload
    image = params[:image].path
    image_results = ImageRecognition.new(image).image_detection
    @breed_info = DogInfo.breed_info(image_results)

    render('index')
  rescue StandardError => e
    Rails.logger.error e.message
    render json: { message: 'Something went wrong try again later' }, status: :internal_server_error
  end
end
