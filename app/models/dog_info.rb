# frozen_string_literal: true

class DogInfo
  def self.dog?(image_results)
    image_results.include?('Dog') || image_results.include?('Dog Breed')
  end

  def self.breed_info(image_results)
    return { "That's probably not a dog, but I sniffed:" => image_results.keys } unless dog?(image_results)

    filter_out_common_results = ['Dog', 'Dog breed', 'Mammal', 'Vertebrate', 'Canidae', 'Nose', 'Carnivore']
    image_results.except!(*filter_out_common_results)
    prime_candidate = image_results.max_by(&:last).first
    result = breeds_that_match(prime_candidate)
    image_results.each { |candidate, _confidence| result.merge(breeds_that_match(candidate)) } if result.empty?
    return { "This is a dog, but I can't tell which breed, must be rare, I sniffed:" => image_results.keys } if result.blank?

    result
  end

  def self.breeds_that_match(candidate)
    dog ||= DogInfoApi.new(url: ENV['DOG_URL'], api_key: ENV['DOG_KEY'])
    result = {}
    dog.breeds.each do |breed|
      result[breed[:name]] = breed if breed[:name].downcase.include?(candidate.downcase)
    end
    result
  rescue StandardError => e
    Rails.logger.info e.message
    {}
  end
end
